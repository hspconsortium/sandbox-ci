#!/usr/bin/env bash

set -x

[[ -z "TARGET_ENV" ]] && { TARGET_ENV="test" }
[[ -z "$AWS_SERVICE_NAME" ]] && { echo "AWS_SERVICE_NAME must be set"; exit 1; } || echo "AWS_SERVICE_NAME: $AWS_SERVICE_NAME"
[[ -z "$AWS_TASK_NAME" ]] && { echo "AWS_TASK_NAME must be set"; exit 1; } || echo "AWS_TASK_NAME: $AWS_TASK_NAME"
[[ -z "$AWS_CONTAINER_NAME" ]] && { echo "AWS_CONTAINER_NAME must be set"; exit 1; } || echo "AWS_CONTAINER_NAME: $AWS_CONTAINER_NAME"

export AWS_REGION=us-east-1
export AWS_ECS_LAUNCH_TYPE=FARGATE
#export AWS_CLUSTER_NAME=sandbox-${TARGET_ENV}
export AWS_SERVICE_NAME=$IMAGE_NAME
export AWS_TASK_NAME=
export AWS_TASK_CPU_UNIT=
export AWS_TASK_MEMORY_UNIT=
export AWS_CONTAINER_NAME=$IMAGE_NAME
export AWS_CONTAINER_PORT=
export AWS_CONTAINER_CPU_UNIT=
export AWS_CONTAINER_MEMORY_UNIT=
export IMAGE_COORDINATES=$IMAGE_NAME
export ENCRYPTION_PASSWORD=
export SPRING_PROFILES_ACTIVE=

