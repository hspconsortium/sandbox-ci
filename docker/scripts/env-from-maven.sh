#!/usr/bin/env bash

[[ -z "$1" ]] && { POM="pom.xml"; } || POM="${1}/pom.xml"

export DOCKER_REPO="hspconsortium"
export DOCKER_IMAGE_NAME=$(mvn -q -f ${POM} -Dexec.executable="echo" -Dexec.args='${project.artifactId}' --non-recursive exec:exec)
export DOCKER_PROJECT_VERSION=$(mvn -q -f ${POM} -Dexec.executable="echo" -Dexec.args='${project.version}' --non-recursive exec:exec)
export DOCKER_IMAGE_COORDINATES="${DOCKER_REPO}/${DOCKER_IMAGE_NAME}:${DOCKER_PROJECT_VERSION}"

echo export DOCKER_REPO=$DOCKER_REPO
echo export DOCKER_IMAGE_NAME=$DOCKER_IMAGE_NAME
echo export DOCKER_PROJECT_VERSION=$DOCKER_PROJECT_VERSION
echo export DOCKER_IMAGE_COORDINATES=$DOCKER_IMAGE_COORDINATES
echo export DOCKER_IMAGE_COORDINATES_ESCAPED=\'$(echo "${DOCKER_IMAGE_COORDINATES}" | sed 's/[][`./'"'"']/\\&/g')\'