#!/usr/bin/env bash

set -e

[[ -z "$1" ]] && { echo "usage: $0 [image coordinates]"; exit 1; } || echo "image coordinates: $1"
[[ -z "$DOCKER_HUB_USERNAME" ]] && { echo "DOCKER_HUB_USERNAME must be set"; exit 1; } || echo "DOCKER_HUB_USERNAME: $DOCKER_HUB_USERNAME"
[[ -z "$DOCKER_HUB_PASSWORD" ]] && { echo "DOCKER_HUB_PASSWORD must be set"; exit 1; } || echo "DOCKER_HUB_PASSWORD: *******"

echo "starting $0..."

# build the Docker image (this will use the Dockerfile in the root of the repo)
docker build -t $1 .

# authenticate with the Docker Hub registry
docker login --username $DOCKER_HUB_USERNAME --password $DOCKER_HUB_PASSWORD

# push the new Docker image to the Docker registry
docker push $1

echo "finished $0"
