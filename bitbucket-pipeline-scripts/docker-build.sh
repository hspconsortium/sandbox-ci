#!/usr/bin/env bash

set -e

[[ -z "$1" ]] && { echo "usage: $0 [image coordinates]"; exit 1; } || echo "image coordinates: $1"

echo "starting $0..."

# build the Docker image (this will use the Dockerfile in the root of the repo)
- docker build -t $1 .

echo "finished $0"
