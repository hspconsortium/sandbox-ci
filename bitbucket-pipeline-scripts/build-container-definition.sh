#!/usr/bin/env bash

set -e

echo "starting $0..."

[[ -z "$1" ]] && { echo "usage: $0 ENCRYPTION_PASSWORD"; exit 1; } || echo "ENV: $1"

# build the container-definitions.json for aws deployments
sed -i -e "s/{{AWS_CONTAINER_NAME}}/$AWS_CONTAINER_NAME/g" sandbox-ci/bitbucket-pipeline-scripts/container-definitions.json
sed -i -e "s/{{DOCKER_REPO}}/$DOCKER_REPO/g" sandbox-ci/bitbucket-pipeline-scripts/container-definitions.json
sed -i -e "s/{{PROJECT_VERSION}}/$PROJECT_VERSION/g" sandbox-ci/bitbucket-pipeline-scripts/container-definitions.json
sed -i -e "s/{{IMAGE_NAME}}/$IMAGE_NAME/g" sandbox-ci/bitbucket-pipeline-scripts/container-definitions.json
sed -i -e "s/{{IMAGE_PORT}}/$IMAGE_PORT/g" sandbox-ci/bitbucket-pipeline-scripts/container-definitions.json
sed -i -e "s/{{IMAGE_MEMORY_RESERVATION}}/$IMAGE_MEMORY_RESERVATION/g" sandbox-ci/bitbucket-pipeline-scripts/container-definitions.json
sed -i -e "s/{{SPRING_PROFILES_ACTIVE}}/$SPRING_PROFILES_ACTIVE/g" sandbox-ci/bitbucket-pipeline-scripts/container-definitions.json
sed -i -e "s/{{ACTIVE_ENV_SM}}/$ACTIVE_ENV_SM/g" sandbox-ci/bitbucket-pipeline-scripts/container-definitions.json
cat sandbox-ci/bitbucket-pipeline-scripts/container-definitions.json
jq '.[].environment += [{"name":"JASYPT_ENCRYPTOR_PASSWORD", "value":"'$1'"}]' sandbox-ci/bitbucket-pipeline-scripts/container-definitions.json > tmp.json && mv tmp.json sandbox-ci/bitbucket-pipeline-scripts/container-definitions.json

echo "finished $0"
