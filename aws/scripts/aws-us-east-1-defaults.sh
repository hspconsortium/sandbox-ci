#!/usr/bin/env bash


[[ -z "$1" ]] && { echo "usage: $0 {project} {test|prod}"; exit 1; }
[[ -z "$2" ]] && { echo "usage: $0 {project} {test|prod}"; exit 1; }

export AWS_TASK_EXECUTION_ROLE="arn:aws:iam::657600230790:role/ecsTaskExecutionRole"

echo export AWS_REGION=us-east-1
echo export AWS_VPC_ID=vpc-858379fe
echo export AWS_SUBNET_1="subnet-5c84cb73"
echo export AWS_SUBNET_2="subnet-c9302782"
echo export AWS_SUBNETS="subnet-5c84cb73,subnet-c9302782"
echo export AWS_SECURITY_GROUPS="sg-46337531"
echo export AWS_TASK_EXECUTION_ROLE="$AWS_TASK_EXECUTION_ROLE"
echo export AWS_TASK_EXECUTION_ROLE_ESCAPED=\'$(echo "${AWS_TASK_EXECUTION_ROLE}" | sed 's/[][`./'"'"']/\\&/g')\'
echo export AWS_ECS_LAUNCH_TYPE=FARGATE
echo export AWS_CLUSTER_NAME="sandbox-$2"
echo export AWS_LOAD_BALANCER_NAME="$1-$2"
echo export AWS_TARGET_GROUP_NAME="$1-$2"
echo export AWS_TARGET_GROUP_PORT=80
echo export AWS_SERVICE_NAME="$1-$2"
echo export AWS_TASK_NAME="$1-$2"
echo export AWS_TASK_CPU_UNIT=256
echo export AWS_TASK_MEMORY_UNIT=1024
echo export AWS_CONTAINER_NAME="$1-$2"
echo export AWS_CONTAINER_PORT=8080
echo export AWS_CONTAINER_CPU_UNIT=256
echo export AWS_CONTAINER_MEMORY_UNIT=1024
echo export AWS_HEALTH_CHECK_PATH="/"
echo export AWS_HEALTH_CHECK_INTERVAL=30
echo export AWS_HEALTH_CHECK_HEALTHY_COUNT=5
echo export AWS_HEALTH_CHECK_UNHEALTHY_COUNT=5
